package com.example.uapv1703110.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);
        final WeatherDbHelper wdb = new WeatherDbHelper(this);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City c = new City();
                c.setName(textName.getText().toString());
                c.setCountry(textCountry.getText().toString());
                wdb.addCity(c);
                finish();
            }
        });
    }


}
