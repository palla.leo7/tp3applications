package com.example.uapv1703110.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,NewCityActivity.class);
                intent.putExtra(City.TAG,(Parcelable[])null);
                startActivity(intent);
            }
        });
        WeatherDbHelper dbHelper=new WeatherDbHelper(this);
        Cursor cursor =dbHelper.fetchAllCities();
        if (!cursor.moveToFirst()) {
            dbHelper.populate();
        }
        cursor =dbHelper.fetchAllCities();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] {WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.COLUMN_COUNTRY},
                new int[] { android.R.id.text1, android.R.id.text2});
        final ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                WeatherDbHelper dbhelper = new WeatherDbHelper(MainActivity.this);
                Cursor c = (Cursor) parent.getItemAtPosition(position);
                City city = dbhelper.cursorToCity(c);
                Intent intent =new Intent(MainActivity.this,CityActivity.class);
                intent.putExtra(City.TAG,city);
                startActivity(intent);
            }
        });/*
        listview.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                AllCityUpdateTask a = new AllCityUpdateTask();
                listview.setRefreshing(false);
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        WeatherDbHelper dbhelper = new WeatherDbHelper(MainActivity.this);
        Cursor cursor = dbhelper.fetchAllCities();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2});
        final ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class AllCityUpdateTask extends AsyncTask<City, Integer, Long> {


        @Override
        protected Long doInBackground(City... citytab) {
            ListView listview = (ListView) findViewById(R.id.listView);
            WeatherDbHelper dbhelper = new WeatherDbHelper(MainActivity.this);
            List<City> cityList = dbhelper.getAllCities();
            for (int i = 0; i < cityList.size(); i++) {
                URL url = null;
                try {
                    url = WebServiceUrl.build(cityList.get(i).getName(), cityList.get(i).getCountry());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    JSONResponseHandler jr = new JSONResponseHandler(cityList.get(i));
                    jr.readJsonStream(in);
                    dbhelper.updateCity(cityList.get(i));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return (long) 1;
        }
    }
}
